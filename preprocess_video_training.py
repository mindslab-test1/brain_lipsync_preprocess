import argparse
import importlib
from itertools import cycle
import logging
from multiprocessing import Pool
import os
import pickle
import sys

import cv2
import torch
from tqdm import tqdm
import yaml

import face_alignment.api as fa
from util.face_landmark import (
    infer_image_3ddfa, infer_image_dlib, infer_image_fa
)
from util.ffmpeg import (
    _ffmpeg_extract_audio, _ffmpeg_extract_first_frame, _ffmpeg_extract_image, _ffmpeg_resample_cut_video
)
from util.util import resize_bbox

sys.path.append('./3ddfa_v2')

logging.getLogger("lipsync-preprocess")


def _extract_landmark_fa(dir_image, dir_landmark, fa_model=None):
    if fa_model is None:
        fa_model = fa.FaceAlignment(fa.LandmarksType._2D)

    img_names = sorted(os.listdir(dir_image))
    for img_name in img_names:
        idx = img_name.split('.')[0]

        path_img = os.path.join(dir_image, img_name)
        img = cv2.imread(path_img)[..., ::-1]  #
        data = infer_image_fa(img, fa_model=fa_model)

        path_landmark = os.path.join(dir_landmark, idx + '.pkl')
        with open(path_landmark, 'wb') as pkl_file:
            pickle.dump(data, pkl_file)


def _extract_landmark_3ddfa(dir_image, dir_landmark, TDDFA, FaceBoxes):
    img_names = sorted(os.listdir(dir_image))
    for img_name in img_names:
        idx = img_name.split('.')[0]

        path_img = os.path.join(dir_image, img_name)
        img = cv2.imread(path_img)  #
        data = infer_image_3ddfa(img, TDDFA, FaceBoxes)

        path_landmark = os.path.join(dir_landmark, idx + '.pkl')
        with open(path_landmark, 'wb') as pkl_file:
            pickle.dump(data, pkl_file)


def preprocess(params):
    vidname, args, device_id = params

    path_video = os.path.join(args.dir_speaker, 'splitted', vidname)
    video_id = vidname.split('.')[0]

    # audio
    dir_audio = os.path.join(args.dir_speaker, 'tts-audio')
    if args.audio:
        os.makedirs(dir_audio, exist_ok=True)
        path_audio = os.path.join(dir_audio, video_id + '.wav')
        _ffmpeg_extract_audio(path_video, path_audio, ar=args.ar)

    # env and model settings
    if args.resample or args.landmark:
        if len(device_id) > 0:
            os.environ['CUDA_VISIBLE_DEVICES'] = device_id
        config_path = './mb1_120x120.yml'
        cfg = yaml.load(open(config_path), Loader=yaml.SafeLoader)
        if args.model == '3ddfa_v2':
            if args.onnx:
                fa_model = TDDFA_module.TDDFA_ONNX(**cfg)
                fb_model = FaceBoxes_module.FaceBoxes_ONNX()
            else:
                if len(device_id) > 0:
                    fa_model = TDDFA_module.TDDFA(gpu_mode=True, gpu_ids=device_id, **cfg)
                else:
                    fa_model = TDDFA_module.TDDFA(gpu_mode=False, **cfg)
                fb_model = FaceBoxes_module.FaceBoxes()
        elif args.model == 'fa':
            fa_model = fa.FaceAlignment(fa.LandmarksType._2D)
            fb_model = None

    # resample
    dir_resample = os.path.join(args.dir_speaker, 'cropped')
    path_resample = os.path.join(dir_resample, video_id + '.mp4')
    if args.resample:
        os.makedirs(dir_resample, exist_ok=True)
        dir_first_frame = os.path.join(args.dir_speaker, 'first_frame')
        os.makedirs(dir_first_frame, exist_ok=True)
        path_first_frame = os.path.join(dir_first_frame, f'{video_id}.png')

        if not args.no_crop:
            _ffmpeg_extract_first_frame(path_video, path_first_frame)
            first_frame = cv2.imread(path_first_frame)
            if args.model == '3ddfa_v2':
                bbox_first = infer_image_3ddfa(first_frame, fa_model, fb_model)['bbox']
            elif args.model == 'fa':
                bbox_first = infer_image_fa(first_frame[..., ::-1], fa_model)['bbox']
            else:
                bbox_first = infer_image_dlib(first_frame[..., ::-1])['bbox']
            bbox_first = resize_bbox(bbox_first, 1.5, 512, first_frame.shape[1], first_frame.shape[0])
        else:
            bbox_first = None

        _ffmpeg_resample_cut_video(path_video, path_resample, crf=12, crop_bbox=bbox_first)

    # image
    dir_image = os.path.join(args.dir_speaker, 'image', video_id)
    if args.image:
        os.makedirs(dir_image, exist_ok=True)
        _ffmpeg_extract_image(path_resample, dir_image, img_name='%05d.png')

    # landmark
    dir_landmark = os.path.join(args.dir_speaker, 'landmark', video_id)
    if args.landmark:
        os.makedirs(dir_landmark, exist_ok=True)

        if args.model == '3ddfa_v2':
            _extract_landmark_3ddfa(dir_image, dir_landmark, TDDFA=fa_model, FaceBoxes=fb_model)
        elif args.model == 'fa':
            _extract_landmark_fa(dir_image, dir_landmark, fa_model=fa_model)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--workers', type=int, default=8)
    parser.add_argument('--gpu_ids', type=str, default='',
                        help='split with comma')
    parser.add_argument('--onnx', action='store_true')

    parser.add_argument('--dir_speaker', type=str, required=True)

    # preprocess related args
    parser.add_argument('--audio', action='store_true',
                        help='if given, extract audio')
    parser.add_argument('--resample', action='store_true',
                        help='resample and cut video')
    parser.add_argument('--image', action='store_true',
                        help='if given, extract image')
    parser.add_argument('--landmark', action='store_true')

    parser.add_argument('--ar', type=int, default=22050,
                        help='audio sample rate')

    # face detection model
    parser.add_argument('--model', choices=['fa', '3ddfa_v2'], default='3ddfa_v2',
                        help='face detection model, ["fa", "3ddfa_v2"]')

    parser.add_argument('--no_crop', action='store_true',
                        help='no use bbox video resampling')

    args = parser.parse_args()
    return args


def main(args):
    # PREPROCESS PIPELINE
    # Resample Video to 25 FPS
    # Extract audio
    # Frame Separation
    # Extract facial landmark and corresponding bbox

    os.makedirs(args.dir_speaker, exist_ok=True)

    vidnames = sorted(
        os.listdir(os.path.join(args.dir_speaker, 'splitted')))  # assert all files are appropriate (video)

    # multiprocess
    device_ids = args.gpu_ids.split(',')
    pool = Pool(processes=args.workers)
    for _ in tqdm(pool.imap_unordered(preprocess, zip(
            vidnames, cycle([args]), cycle(device_ids)
    ))):
        pass


if __name__ == '__main__':
    args = parse_args()
    if args.onnx:
        TDDFA_module = importlib.import_module('.TDDFA_ONNX', '3ddfa_v2')
        FaceBoxes_module = importlib.import_module('.FaceBoxes.FaceBoxes_ONNX', '3ddfa_v2')

    else:
        TDDFA_module = importlib.import_module('.TDDFA', '3ddfa_v2')
        FaceBoxes_module = importlib.import_module('.FaceBoxes.FaceBoxes', '3ddfa_v2')

    main(args)
