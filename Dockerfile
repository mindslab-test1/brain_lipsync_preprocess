FROM docker.maum.ai:443/brain/vision:v0.3.1

COPY . /app
RUN pip install -r /app/3ddfa_v2/requirements.txt && \
    pip install -r /app/requirements.txt && \
    cd /app/3ddfa_v2 && bash build.sh && \
    mkdir -p /root/.cache/torch/hub && mv /app/checkpoints /root/.cache/torch/hub/
#    wget https://www.adrianbulat.com/downloads/python-fan/s3fd-619a316812.pth -O /root/.cache/torch/hub/checkpoints/s3fd-619a316812.pth && \
#    wget https://www.adrianbulat.com/downloads/python-fan/2DFAN4-11f355bf06.pth.tar -O /root/.cache/torch/hub/checkpoints/2DFAN4-11f355bf06.pth.tar

WORKDIR /app

EXPOSE 43000 43001
