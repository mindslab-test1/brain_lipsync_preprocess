# Brain-Vision-Video-Process

본 Repo는 Brain-Vision 내에서 Lipsync용 Video 데이터에 대한 Lipsync 전처리를 하는 데 필요한 함수들을 모아놓기 위해 작성했습니다.

## docker image build
* 이미지 빌드 전 모든 submodule을 update해주세요
* 다음 두 checkpoint를 checkpoints 안에 격납해주세요
  * https://www.adrianbulat.com/downloads/python-fan/s3fd-619a316812.pth
  * https://www.adrianbulat.com/downloads/python-fan/2DFAN4-11f355bf06.pth.tar

## 전처리 순서
1. Original Video들을 `YOUR_DATA_PATH/original`안에 위치시킵니다.
  a. YOUR_DATA_PATH 에는 speaker_id와 version이 디렉토리로 구분될 수 있도록 구성합니다. 
  b. speaker_id: 사람 별 디렉토리 (ex: sora, pcm, teacher_woman, ...)
  c. version: 동일 사람에 대해 다른 데이터 분류 (ex: sora_02, sora_03은 sora/02, sora/03에 각각 격납)
2. vad.py를 이용해
  a. vad detection에 사용할 오디오를 추출합니다.
    ㄱ. 예시: `python vad.py --mode extract_audio --video_path YOUR_DATA_PATH/original --wav_path YOUR_DATA_PATH/vad_wav`
  b. vad detection을 진행합니다.
    ㄱ. 예시: `python vad.py --mode vad_detection --wav_path YOUR_DATA_PATH/vad_wav --vad_path YOUR_DATA_PATH/vad_result`
  c. vad 결과를 이용해 비디오를 분할합니다.
    ㄱ. 예시: `python vad.py --mode split --video_path YOUR_DATA_PATH/original --vad_path YOUR_DATA_PATH/vad_result --workers 1`
3. splitted된 비디오를 검수합니다.  
  a. `YOUR_DATA_PATH/splitted` 디렉토리 안에 분할된 비디오들이 저장됩니다.
  b. 사용할 수 없는 비디오를 검수 및 제거할 때에는 `어떤 영상을 제거했는지` 등의 정보를 기록해둡니다.
4. resample ([`preprocess_video_training.py`](./preprocess_video_training.py))   
  b. --dir_speaker: `YOUR_DATA_PATH/speaker_id/version` 경로 입력  
  c. --model: 현재(v1.1.6) 최신 모델인 "3ddfa_v2" 입력  
  d. gpu 메모리가 터지는 문제가 발생할 경우 --workers 수를 줄인다(default=8)  
  e. 각 세부 process에 대해서 진행하고 싶은 process에 대한 args를 주면 됩니다.(`--resample`, `--audio`, `--image`, `--landmark`)


## 데이터 스펙 정리
* original videos:
    *  requirement: MP4 format,
* extract_audio.sh로 뽑은 original audio:
    * sample_rate=48000
* vad_detection.sh로 뽑은 splitted video:
    * ffmpeg -crf=12, fps=25, video idx=05d(starts from 00000), format=mp4
* preprocess_video_training.sh로 뽑은:
    * resample.mp4:
        * ffmpeg crf=12, fps=25, 3ddfa bbox의 가로/세로 각 1.5배로 crop됨, format=mp4
    * audio.wav:
        * sample_rate=22050, mono channel, format=wav
    * image:
        * 각 vid별 디렉토리 생성 후 저장
        * format=png, idx=05d(starts from 00001)
    * landmark:
        * 각 vid별 디렉토리 생성 후 저장
        * format=pkl (save python dict with python pickle module)
        * 모든 bbox는 landmark로부터 계산할 수 있음
        * face detection 과정에서 문제 발생할 경우 landmark=None으로 저장됨.

## TODO
* error handling
* ffmpeg command check
* audio 저장 경로를 tts-audio에서 audio로 바꾸기... (합의 필요)

