"""
This code and voice_activity_detection is based on rVAD.
rVAD: An Unsupervised Segment-Based Robust Voice Activity Detection Method
Author's code can be found at https://github.com/zhenghuatan/rVAD/tree/master/rVADfast_py_2.0.
"""

from __future__ import division
import argparse
from copy import deepcopy
from itertools import cycle
import json
from multiprocessing import Pool
import os
import subprocess

import librosa
import numpy as np
from scipy.signal import lfilter
from tqdm import tqdm, trange

from util.ffmpeg import _ffmpeg_extract_audio
from util.util import sec_to_timestamp, timestamp_to_sec
import voice_activity_detection.speechproc as speechproc

WIN_LEN = 0.025
OVR_LEN = 0.01
PRE_COEF = 0.97
N_FILTER = 20
N_FTT = 512
FT_TRHES = 0.5
VAD_THRES = 0.4
ENERGYFLOOR = np.exp(-50)

OPTS = 1


# region extract


def extract_audio(params):
    video_file, args = params

    viddir = os.path.dirname(video_file)
    vidname = os.path.basename(video_file)
    vidname = vidname.rsplit('.', 1)[0]

    os.makedirs(args.wav_path, exist_ok=True)
    wav_file = os.path.join(args.wav_path, vidname + '.wav')

    assert video_file != wav_file
    _ffmpeg_extract_audio(video_file, wav_file, ar=48000)


# endregion


# region detection
def get_sflux_result(speech_handler):
    fs, data = speech_handler.speech2wav()
    ft, flen, fsh10, nfr10 = speechproc.sflux(data, fs, WIN_LEN, OVR_LEN, N_FTT)
    return {'data': data, 'nfr10': nfr10, 'flen': flen, 'fsh10': fsh10, 'ft': ft}


def get_pitch_block(fin_sflux):
    # --spectral flatness --
    pv01 = np.zeros(fin_sflux['nfr10'])
    pv01[np.less_equal(fin_sflux['ft'], FT_TRHES)] = 1
    pitch = deepcopy(fin_sflux['ft'])

    pvblk = speechproc.pitchblockdetect(pv01, pitch, fin_sflux['nfr10'], OPTS)
    return {'pv01': pv01, 'pvblk': pvblk}


# Get IIR (or FIR) filter and apply
def get_lpf_result(fin_sflux):
    b = np.array([0.9770, -0.9770])
    a = np.array([1.0000, -0.9540])
    fdata = lfilter(b, a, fin_sflux['data'], axis=0)
    return fdata


# Apply LPF for de-noise
def supress_noise(fin_sflux, pitch_block, fdata):
    noise_samp, noise_seg, n_noise_samp = speechproc.snre_highenergy(
        fdata, fin_sflux['nfr10'], fin_sflux['flen'], fin_sflux['fsh10'], ENERGYFLOOR,
        pitch_block['pv01'], pitch_block['pvblk'])

    # Set noisy segments to zero
    for j in range(n_noise_samp):
        fdata[range(int(noise_samp[j, 0]), int(noise_samp[j, 1]) + 1)] = 0

    return fdata


def snre_vad(fin_sflux, pitch_block, fdata, wav_path, txt_path=None):
    vad_seg = speechproc.snre_vad(fdata, fin_sflux['nfr10'], fin_sflux['flen'], fin_sflux['fsh10'], ENERGYFLOOR,
                                  pitch_block['pv01'], pitch_block['pvblk'], VAD_THRES)

    vad_seg = vad_seg.astype(int)
    if txt_path is not None:
        np.savetxt(txt_path, vad_seg, fmt='%i')
        print("%s --> %s " % (wav_path, txt_path))

    return vad_seg


def vad_detection(params):
    wav_file, args = params
    os.makedirs(args.vad_path, exist_ok=True)
    filename = os.path.basename(wav_file).rsplit('.', 1)[0]

    speech_handler = speechproc.SpeechLoader(wav_file, samplerate=48000)

    fin_sflux = get_sflux_result(speech_handler)
    pitch_block = get_pitch_block(fin_sflux)
    wav_fdata = get_lpf_result(fin_sflux)

    wav_denoised_fdata = supress_noise(fin_sflux, pitch_block, wav_fdata)
    vad_path = os.path.join(args.vad_path, filename + '.txt')
    vad_seg = snre_vad(fin_sflux, pitch_block, wav_denoised_fdata, wav_file, vad_path)

    voice_timecode = get_timecode_from_vad(vad_seg)
    timecode_np = np.array(voice_timecode).reshape((-1, 2))
    print(wav_file, timecode_np)

    return vad_seg


# endregion
def get_timecode_from_vad(vad_seg):
    timecode = list()
    was_voice = False
    silence_length = 0
    for idx, _vad in enumerate(vad_seg):
        if _vad == 1 and not was_voice:  # Silence end
            if silence_length < 100 and len(timecode) != 0:  # If silence is not that long,
                timecode.pop()  # keep it as a single segment
            else:  # If silence is long enough
                timecode.append(idx)  # append start_idx
            silence_length = 0  # reset silence_length
            was_voice = True
        elif _vad == 0 and was_voice:
            silence_length += 1
            timecode.append(idx)  # append end_idx
            was_voice = False
        elif _vad == 0 and not was_voice:
            silence_length += 1

    if was_voice:  # if done with voice active,
        timecode.append(len(vad_seg))  # append last index

    real_timecode = list()
    for _idx in range(len(timecode) // 2):
        if timecode[_idx * 2 + 1] - timecode[_idx * 2] >= 100:
            real_timecode.extend(timecode[_idx * 2:(_idx + 1) * 2])

    return real_timecode


# region split
def chunks(timecode_np, n):
    for i in range(0, timecode_np.shape[0], n):
        yield timecode_np[i:i + n, :]


def pad_timecode(start_time_str, duration_time_str, padding=None):
    """
    padding should be ginve with sec
    """

    start_time = timestamp_to_sec(start_time_str)
    start_time = max(0, start_time - padding[0])
    start_time_str = sec_to_timestamp(start_time)

    duration_time = timestamp_to_sec(duration_time_str)
    duration_time = duration_time + padding[-1]
    duration_time_str = sec_to_timestamp(duration_time)

    return start_time_str, duration_time_str


def split(params):
    video_file, vad_file, args = params

    vad_seg = np.loadtxt(vad_file)
    voice_timecode = get_timecode_from_vad(vad_seg)
    assert len(voice_timecode) % 2 == 0
    timecode_np = np.array(voice_timecode).reshape((-1, 2))
    print(timecode_np.shape)

    speaker_dir = os.path.dirname(os.path.dirname(video_file))
    save_dir = os.path.join(speaker_dir, 'splitted')
    os.makedirs(save_dir, exist_ok=True)

    file_idx = 0
    filename = os.path.basename(video_file)
    filename = filename.rsplit('.', 1)[0]

    timestamps = []
    for timecode_chunk in list(chunks(timecode_np, args.chunk_size)):
        ffmpeg_command = ['ffmpeg', '-y', '-loglevel', 'error', '-i', video_file]
        file_extension = video_file.split('.')[-1]
        for idx, _segment in enumerate(timecode_chunk, file_idx):
            split_video_name = f'{filename}_{idx:05d}.{file_extension}'
            path_split_save = os.path.join(save_dir, split_video_name)

            start_time_str, duration_time_str = speechproc.get_video_timecode(_segment[0], _segment[1])
            print(start_time_str, duration_time_str)

            timestamp_log = {
                'original file': video_file,
                'splitted file': path_split_save,
                'timestamp start': start_time_str,
                'duration': duration_time_str
            }
            timestamps.append(timestamp_log)

            start_time_str, duration_time_str = pad_timecode(
                start_time_str, duration_time_str, padding=args.vad_padding)
            ffmpeg_command.extend(['-ss', start_time_str, '-t', duration_time_str,
                                   '-c:v', 'libx264', '-strict', '-2', '-crf', '12',
                                   path_split_save])

        ffmpeg_command = " ".join(ffmpeg_command)
        subprocess.call(ffmpeg_command, shell=True)

        file_idx = idx + 1

    dir_timestamp = os.path.join(speaker_dir, 'timestamps')
    os.makedirs(dir_timestamp, exist_ok=True)
    path_timestamp = os.path.join(dir_timestamp, f'{filename}.json')
    with open(path_timestamp, 'w') as f:
        json.dump(timestamps, f, indent=4)


# endregion

def main():
    args = parse_args()

    if args.mode == 'extract_audio':
        assert args.video_path
        if os.path.isdir(args.video_path):
            video_files = sorted([os.path.join(args.video_path, file) for file in os.listdir(args.video_path)])
        elif os.path.isfile(args.video_path):
            video_files = [args.video_path]
        else:
            print('not supported')
            return

        pool = Pool(processes=args.workers)
        for _ in tqdm(pool.imap_unordered(extract_audio, zip(
                video_files, cycle([args])
        ))):
            pass

    if args.mode == 'vad_detection':
        assert args.wav_path
        if os.path.isdir(args.wav_path):
            wav_files = sorted([os.path.join(args.wav_path, file) for file in os.listdir(args.wav_path)
                                if file.lower().endswith('.wav')])
        elif os.path.isfile(args.wav_path) and args.wav_path.lower().endswith('.wav'):
            wav_files = [args.wav_path]
        else:
            print('not supported')
            return

        pool = Pool(processes=args.workers)
        for _ in tqdm(pool.imap_unordered(vad_detection, zip(
                wav_files, cycle([args])
        ))):
            pass

    if args.mode == 'split':
        assert args.video_path
        assert args.vad_path

        if args.workers != 1:
            print('setting args.workers to 1')
            args.workers = 1

        if os.path.isdir(args.video_path):
            assert os.path.isdir(args.vad_path)
            video_files = sorted([os.path.join(args.video_path, file) for file in os.listdir(args.video_path)])
            vad_files = sorted([os.path.join(args.vad_path, file) for file in os.listdir(args.vad_path)])
        elif os.path.isfile(args.video_path) and os.path.isfile(args.vad_path):
            video_files = [args.video_path]
            vad_files = [args.vad_path]
        else:
            print('not supported')
            return

        pool = Pool(processes=args.workers)
        for _ in tqdm(pool.imap_unordered(split, zip(
                video_files, vad_files, cycle([args])
        ))):
            pass


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--mode', choices=['extract_audio', 'vad_detection', 'split'])

    parser.add_argument('-v', "--video_path", type=str,
                        help="video filepath")
    parser.add_argument('-w', "--wav_path", type=str,
                        help="audio filepath")
    parser.add_argument('--vad_path', type=str,
                        help="vad result filepath")

    parser.add_argument("--vad_padding", type=tuple, default=(1.0, 1.0),
                        help="vad padding time, used only at split mode")

    parser.add_argument('--chunk_size', type=int, default=10,
                        help='ffmpeg chunk size')
    parser.add_argument('--workers', type=int, default=4,
                        help='')

    args = parser.parse_args()
    return args


if __name__ == '__main__':
    main()
