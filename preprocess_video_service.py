import os
import pickle as pkl
from util.face_landmark import get_bbox_from_landmark, get_image_from_landmark
from tqdm import tqdm
from multiprocessing import Pool
from itertools import cycle
import face_alignment.api as face_alignment
import cv2
import argparse

import logging
from pathlib import Path

logging.getLogger("lipsync-preprocess")


def main(args):
    # Resample Video to 25 FPS
    # Resize if it is too big (at most 1920 x 1080)
    # Check the resolution of video

    # Frame Separation

    # Extract facial landmark and corresponding bbox
