import os
import pickle as pkl
import sys

import cv2
import dlib
import numpy as np
import torch


def _set_midpoint(landmark_data):
    jaw_mid = (landmark_data[1] + landmark_data[15]) // 2
    # return (landmark_data[33] + jaw_mid) // 2
    return jaw_mid


def _get_ratio(landmark_data):
    jaw_vector = landmark_data[16] - landmark_data[0]
    jaw_dist = np.linalg.norm(jaw_vector)
    return jaw_dist, jaw_vector[0]


def get_bbox_from_landmark(image_path, ratio=0.4, crop_size=256, fa_model=None):
    if isinstance(image_path, str):
        image_path = cv2.imread(image_path)
    with torch.no_grad():
        landmark = fa_model.get_landmarks(image_path)
    landmark = np.array(landmark[0])
    midpoint = _set_midpoint(landmark)
    _, jaw_x = _get_ratio(landmark)

    crop_half = int((jaw_x / ratio) * 0.5)

    bbox = [int(midpoint[0] - crop_half),
            int(midpoint[1] - crop_half),
            int(midpoint[0] + crop_half),
            int(midpoint[1] + crop_half)]

    return landmark, bbox


def get_image_from_landmark(image_path, pkl_path, cropped_dir):
    im = cv2.imread(image_path)
    with open(pkl_path, 'rb') as f:
        data = pkl.load(f)

    bbox = data['bbox']
    cropped_image_path = os.path.join(cropped_dir, os.path.basename(image_path))
    cropped_image = im[bbox[1]:bbox[3], bbox[0]:bbox[2], :]
    cv2.imwrite(cropped_image_path, cropped_image)

    return cropped_image


def infer_image_3ddfa(img, TDDFA, FaceBoxes):
    try:
        with torch.no_grad():
            boxes = FaceBoxes(img)
            n = len(boxes)
            if n == 0:
                roi_box_lst = [[0, 0, img.shape[1], img.shape[0]]]
                ver_lst = [None]
            else:
                param_lst, roi_box_lst = TDDFA(img, boxes)
                ver_lst = TDDFA.recon_vers(param_lst, roi_box_lst, dense_flag=False)
            roi_box_lst = roi_box_lst[0]
            ver_lst = ver_lst[0]
    except Exception as e:
        print(e)
        ver_lst = None
        roi_box_lst = [0, 0, img.shape[1], img.shape[0]]

    data = {
        'landmark': ver_lst,
        'bbox': roi_box_lst
    }

    return data


def infer_image_fa(img, fa_model):
    try:
        landmark, bbox = get_bbox_from_landmark(img, fa_model=fa_model, ratio=0.4)
        if bbox[0] < 0:            bbox[0] = 0
        if bbox[1] < 0:            bbox[1] = 0
        if bbox[2] > img.shape[1]: bbox[2] = img.shape[1]
        if bbox[3] > img.shape[0]: bbox[3] = img.shape[0]
    except Exception as e:
        print(e)
        landmark = None
        bbox = None

    data = {
        'landmark': landmark,
        'bbox': bbox
    }

    return data


def infer_image_dlib(img):
    face_detector = dlib.get_frontal_face_detector()
    try:
        faces = face_detector(img)
        face = faces[0]
        x1, y1, x2, y2 = face.left(), face.top(), face.right(), face.bottom()
        bbox = [x1, y1, x2, y2]
    except Exception as e:
        bbox = None

    data = {
        'bbox': bbox
    }

    return data
