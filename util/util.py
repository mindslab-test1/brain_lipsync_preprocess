# https://pms.maum.ai/bitbucket/users/dongho.choi_mindslab.ai/repos/dataset_downloader/browse/utils/util.py

def _frame_to_time(frame, fps):
    frame_in_time = frame / fps
    h = int(frame_in_time // 60 // 60)
    m = int(frame_in_time // 60 % 60)
    s = frame_in_time % 60
    # ms = frame_in_time % 1

    return f'{h}:{m}:{s}'


def resize_bbox(bbox, ratio: float, min_size: int, max_w: int, max_h: int):
    x1, y1, x2, y2 = bbox

    # resize with ratio
    if ratio < 0:
        ratio = 0
    xd = (x2 - x1)
    x1 -= xd * (ratio - 1) / 2.0
    x2 += xd * (ratio - 1) / 2.0

    yd = y2 - y1
    y1 -= yd * (ratio - 1) / 2.0
    y2 += yd * (ratio - 1) / 2.0

    # check minsize
    xd = x2 - x1
    if xd < min_size:
        x1 -= (min_size - xd) / 2.0
        x2 += (min_size - xd) / 2.0

    yd = y2 - y1
    if yd < min_size:
        y1 -= (min_size - yd) / 2.0
        y2 += (min_size - yd) / 2.0

    # check img shape vs bbox
    if x1 < 0: x1 = 0
    if y1 < 0: y1 = 0
    if x2 > max_w: x2 = max_w
    if y2 > max_h: y2 = max_h

    bbox = [x1, y1, x2, y2]
    return bbox


def timestamp_to_sec(timestamp):
    nums = timestamp.split(':')
    assert len(nums) == 3
    h = int(nums[0])
    m = int(nums[1])
    s, ms = nums[2].split('.')
    s = int(s)
    ms = int(ms)

    sec = h * 3600 + m * 60 + s + ms / 1000.
    return sec


def sec_to_timestamp(sec):
    h = int(sec // 3600)
    m = int(sec % 3600 // 60)
    s = int(sec % 60 // 1)
    ms = sec % 1

    timestamp = f'{h:02d}:{m:02d}:{s:02d}.{int(ms * 1000):03d}'
    return timestamp
