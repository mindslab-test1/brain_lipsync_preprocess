# modified from
# https://pms.maum.ai/bitbucket/users/dongho.choi_mindslab.ai/repos/dataset_downloader/browse/utils/ffmpeg.py

import os
import subprocess


def _ffmpeg_extract_first_frame(path_input: str, path_output: str):
    ffmpeg_commands = [
        'ffmpeg', '-y',
        '-hide_banner',
        '-loglevel', 'error',
        '-i', path_input,

        '-vframes', '1',

        path_output
    ]
    ffmpeg_command = ' '.join(ffmpeg_commands)
    subprocess.call(ffmpeg_command, shell=True)


def _ffmpeg_resample_cut_video(path_input: str, path_output: str, fps=25, crf=0, crop_bbox=None):
    ffmpeg_commands = [
        'ffmpeg', '-y',
        '-hide_banner',
        '-loglevel', 'error',
        '-i', path_input,

        '-r', f'{fps}',  # video parameters
        '-vcodec', 'libx264',
        '-crf', f'{crf}',

        '-ar', '44100',  # audio parameters
        '-ac', '1',
        '-strict', '-2',
    ]
    if crop_bbox is not None:
        ffmpeg_bbox = [int(item) for item in crop_bbox]
        ffmpeg_bbox = f'{ffmpeg_bbox[2] - ffmpeg_bbox[0]}:{ffmpeg_bbox[3] - ffmpeg_bbox[1]}:{ffmpeg_bbox[0]}:{ffmpeg_bbox[1]}'
        ffmpeg_commands.extend([
            '-vf', f'"crop={ffmpeg_bbox}"'
        ])
    ffmpeg_commands.append(path_output)
    ffmpeg_command = ' '.join(ffmpeg_commands)
    subprocess.call(ffmpeg_command, shell=True)


def _ffmpeg_extract_audio(path_input: str, path_output: str, ar: int=22050, ac: int=1):
    ffmpeg_commands = [
        'ffmpeg', '-y',
        '-hide_banner',
        '-loglevel', 'error',
        '-i', path_input,

        '-ar', f'{ar}',
        '-ac', f'{ac}',
        path_output
    ]
    ffmpeg_command = ' '.join(ffmpeg_commands)
    subprocess.call(ffmpeg_command, shell=True)


def _ffmpeg_extract_image(path_input: str, dir_output: str, img_name: str = '%05d.jpg'):
    os.makedirs(dir_output, exist_ok=True)
    ffmpeg_commands = [
        'ffmpeg', '-y',
        '-hide_banner',
        '-loglevel', 'error',
        '-i', path_input,

        f'{dir_output}/{img_name}'
    ]
    ffmpeg_command = ' '.join(ffmpeg_commands)
    subprocess.call(ffmpeg_command, shell=True)

    return dir_output
